const { Router } = require('express')
const router = Router()

const User = require('../models/user.js')
const argon2 = require('argon2')
const jwt = require('jsonwebtoken')
const { checkAuth } = require('../middleware/passport.js')
const connectRabbitMQ = require('../config/rabbitmq.js')

router.get('/', checkAuth, async function (req, res) {
  const users = await User.findAll({ attributes: ['id', 'fullname', 'email', 'role'] })
  res.json({
    users,
  })
})

router.get('/profile', checkAuth, async function (req, res) {
  const user = await User.findByPk(req.user.user_id)
  res.json({
    user: {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      role: user.role,
    },
  })
})

router.post('/register', async (req, res) => {
  try {
    const { fullname, email, password } = req.body
    const exist = await User.findOne({ where: { email } })
    if (exist) {
      return res.status(400).json({
        message: 'email is already exist',
      })
    }
    const hash = await argon2.hash(password)

    const user = await User.create({
      fullname,
      email,
      password: hash,
    })

    // connect RabbitMQ and create exchange
    const channel = await connectRabbitMQ()
    await channel.assertExchange('ex.sing.fanout', 'fanout', { durable: true })
    await channel.assertExchange('ex.sing.direct', 'direct', { durable: true })
    // send data User to product-server
    await channel.assertQueue('q.sing.product.service', { durable: true })
    await channel.bindQueue('q.sing.product.service', 'ex.sing.fanout', '')

    // send data User to order-server
    await channel.assertQueue('q.sing.order.service', { durable: true })
    await channel.bindQueue('q.sing.order.service', 'ex.sing.fanout', '')

    // send data User to order-server
    await channel.assertQueue('q.sing.order.report.service', { durable: true })
    await channel.bindQueue('q.sing.order.report.service', 'ex.sing.fanout', '')

    const newUser = {
      id: user.id,
      fullname: user.fullname,
      email: user.email,
      role: user.role,
    }

    channel.publish('ex.sing.fanout', '', Buffer.from(JSON.stringify(newUser)), {
      contentType: 'application/json',
      contentEncoding: 'utf-8',
      type: 'UserCreated',
      // persistent: true,
    })

    // direct exchange
    await channel.assertQueue('q.sing.direct.product.service', { durable: true })
    // await channel.bindQueue('q.sing.direct.product.service', 'ex.sing.direct', '')
    channel.publish('ex.sing.direct', 'r.sing.product.service', Buffer.from(JSON.stringify(newUser)), {
      contentType: 'application/json',
      contentEncoding: 'utf-8',
      type: 'UserCreated-Direct',
      persistent: true,
    })
    res.status(201).json({
      message: 'ลงทะเบียนสำเร็จ',
      user: { id: user.id },
    })
  } catch (error) {
    console.error(error)
  }
})

router.post('/login', async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ where: { email } })

  if (!user) {
    return res.status(401).json({
      message: 'Invalid email or password',
    })
  }

  const valid = await argon2.verify(user.password, password)
  if (!valid) {
    return res.status(401).json({
      message: 'Invalid email or password',
    })
  }

  const payload = { sub: user.id, role: user.role, issuer: process.env.JWT_ISSUER }

  const token = jwt.sign(payload, process.env.JWT_SECRET, {
    algorithm: 'HS256',
    expiresIn: '1h',
  })

  res.status(200).json({
    access_token: token,
  })
})

module.exports = router
