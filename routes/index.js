const { Router } = require('express')
const router = Router()

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Backend' })
})
router.get('/healthz', (req, res) => {})

module.exports = router
