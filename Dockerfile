FROM node:lts-alpine as build
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY ["package.json",  "./"]
RUN npm install --production --silent
COPY . .


FROM node:lts-alpine
WORKDIR /app/backend
RUN npm install -g pm2  
COPY --from=build /usr/src/app /app/backend
EXPOSE 4000

RUN chown -R node /app/backend
USER node

CMD ["npm", "run", "prod"]