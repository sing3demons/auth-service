const passport = require('passport')
const { Strategy: JwtStrategy, ExtractJwt } = require('passport-jwt')

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = process.env.JWT_SECRET
opts.issuer = process.env.JWT_ISSUER
// opts.audience = 'yoursite.net';

passport.use(
  new JwtStrategy(opts, async function ({ sub }, done) {
    try {
      const user = { user_id: sub }
      if (user) {
        return done(null, user) // ส่งข้อมูล user ไปกับ req
      }
    } catch (error) {
      done(error)
    }
  })
)

// check authen
module.exports.checkAuth = passport.authenticate('jwt', { session: false })
